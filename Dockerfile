FROM python:3.9.15-slim

RUN apt-get update -qq 
RUN pip install --upgrade pip
RUN pip install asgiref==3.4.1
RUN pip install Django==3.2.9
RUN pip install gunicorn==20.1.0
RUN pip install psycopg2-binary==2.9.2
RUN pip install pytz==2021.3
RUN pip install sqlparse==0.4.2
RUN pip install whitenoise==5.3.0

RUN mkdir /myapp
WORKDIR /myapp

COPY clinic /myapp

COPY docker-entrypoint.sh /usr/bin/
RUN chmod +x /usr/bin/docker-entrypoint.sh
ENTRYPOINT ["docker-entrypoint.sh"]
EXPOSE 9000


